﻿using GroceryList_MVVM.Models;
using Microsoft.EntityFrameworkCore;

namespace GroceryList_MVVM.Data;

public class GroceriesListContext : DbContext
{
	public GroceriesListContext(DbContextOptions<GroceriesListContext> options) : base(options)
	{
	}

	public DbSet<Grocery> Groceries { get; set; }
	public DbSet<Category> Categories { get; set; }
}
