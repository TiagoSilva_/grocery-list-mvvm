﻿namespace GroceryList_MVVM.Models;

public class Category
{
    public int Id { get; set; }
    public string Name { get; set; }
    public List<Grocery> Groceries { get; set; }
}
