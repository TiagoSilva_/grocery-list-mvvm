﻿ namespace GroceryList_MVVM.Models;

public class Grocery
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string? Description { get; set; }
    public int Quantity { get; set; }
    public Category? Category { get; set; }
    public int CategoryId { get; set; }
}
