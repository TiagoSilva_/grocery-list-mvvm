﻿using System.Linq.Expressions;

namespace GroceryList_MVVM.Repository;

public interface IRepository<T> where T : class
{
    Task Add(T entity);
    Task<IEnumerable<T>> GetAll(Expression<Func<T, bool>> expression = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, params string[] includes);
    Task<T> Get(Expression<Func<T, bool>> expression = null, params string[] includes);
    Task Remove(T entity);
    Task Save();
    Task Update(T entity);
    Task<bool> Exists(Expression<Func<T, bool>> expression = null);
}