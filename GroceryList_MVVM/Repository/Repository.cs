﻿using GroceryList_MVVM.Data;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace GroceryList_MVVM.Repository;

public class Repository<T> : IRepository<T> where T : class
{
    protected readonly GroceriesListContext _context;

    public Repository(GroceriesListContext context)
    {
        _context = context;
    }

    public async Task Add(T entity)
    {
        await _context.Set<T>().AddAsync(entity);
        await Save();
    }

    public async Task<IEnumerable<T>> GetAll(Expression<Func<T, bool>> expression = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, params string[] includes)
    {
        IQueryable<T> query = _context.Set<T>();

        if (expression != null)
        {
            query = query.Where(expression);
        }

        if (includes != null)
        {
            foreach (var table in includes)
            {
                query = query.Include(table);
            }
        }

        if (orderBy != null)
        {
            query = orderBy(query);
        }

        return await query.ToListAsync();
    }

    public async Task<T> Get(Expression<Func<T, bool>> expression = null, params string[] includes)
    {
        IQueryable<T> query = _context.Set<T>();
        if (includes != null)
        {
            foreach (var table in includes)
            {
                query = query.Include(table);
            }
        }
        return await query.FirstOrDefaultAsync(expression);
    }

    public async Task Remove(T entity)
    {
        _context.Set<T>().Remove(entity);
        await Save();
    }

    public async Task Update(T entity)
    {
        _context.Set<T>().Update(entity);
        await Save();
    }

    public async Task Save()
    {
        await _context.SaveChangesAsync();
    }

    public async Task<bool> Exists(Expression<Func<T, bool>> expression = null)
    {
        IQueryable<T> query = _context.Set<T>();
        return await query.AnyAsync(expression);
    }
}
