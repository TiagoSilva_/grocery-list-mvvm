﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GroceryList_MVVM.Data;
using GroceryList_MVVM.Models;
using GroceryList_MVVM.Repository;

namespace GroceryList_MVVM.Controllers
{
    public class GroceriesController : Controller
    {
        private readonly IRepository<Grocery> _groceryRepository;
        private readonly IRepository<Category> _categoryRepository;

        public GroceriesController(IRepository<Grocery> groceryRepository, IRepository<Category> categoryRepository)
        {
            _groceryRepository = groceryRepository;
            _categoryRepository = categoryRepository;
        }

        // GET: Groceries
        public async Task<IActionResult> Index()
        {
            if (_groceryRepository == null)
            {
                return NotFound();
            }

            var groceries = await _groceryRepository.GetAll(includes: "Category");

            return View(groceries);
        }

        // GET: Groceries/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _groceryRepository == null)
            {
                return NotFound();
            }

            var grocery = await _groceryRepository.Get(grocery => grocery.Id == id, "Category" );

            if (grocery == null)
            {
                return NotFound();
            }

            return View(grocery);
        }

        // GET: Groceries/Create
        public async Task<IActionResult> Create()
        {
            ViewData["CategoryId"] = new SelectList(await _categoryRepository.GetAll(), nameof(Category.Id), nameof(Category.Name));
            return View();
        }

        // POST: Groceries/Create
        [HttpPost]
        public async Task<IActionResult> Create([Bind("Id,Name,Description,Quantity,CreatedAt,UpdatedAt,CategoryId")] Grocery grocery)
        {
            if (ModelState.IsValid)
            {
                await _groceryRepository.Add(grocery);
                return RedirectToAction(nameof(Index));
            }
            ViewData["CategoryId"] = new SelectList(await _categoryRepository.GetAll(), nameof(Category.Id), nameof(Category.Name));
            return View(grocery);
        }

        // GET: Groceries/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _groceryRepository == null)
            {
                return NotFound();
            }

            var grocery = await _groceryRepository.Get(grocery => grocery.Id == id, "Category");
            if (grocery == null)
            {
                return NotFound();
            }
            ViewData["CategoryId"] = new SelectList(await _categoryRepository.GetAll(), nameof(Category.Id), nameof(Category.Name));
            return View(grocery);
        }

        // POST: Groceries/Edit/5
        [HttpPost]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Description,Quantity,CreatedAt,UpdatedAt,CategoryId")] Grocery grocery)
        {
            if (id != grocery.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await _groceryRepository.Update(grocery);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!await _groceryRepository.Exists(grocery => grocery.Id == id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }   
                return RedirectToAction(nameof(Index));
            }
            ViewData["CategoryId"] = new SelectList(await _categoryRepository.GetAll(), nameof(Category.Id), nameof(Category.Name));
            return View(grocery);
        }

        // GET: Groceries/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _groceryRepository == null)
            {
                return NotFound();
            }

            var grocery = await _groceryRepository.Get(grocery => grocery.Id == id, "Category");
            if (grocery == null)
            {
                return NotFound();
            }

            return View(grocery);
        }

        // POST: Groceries/Delete/5
        [HttpPost, ActionName("Delete")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_groceryRepository == null)
            {
                return Problem("Entity set 'GroceriesListContext.Groceries'  is null.");
            }
            var grocery = await _groceryRepository.Get(grocery => grocery.Id == id, "Category");
            if (grocery != null)
            {
                await _groceryRepository.Remove(grocery);
            }
            
            return RedirectToAction(nameof(Index));
        }
    }
}
